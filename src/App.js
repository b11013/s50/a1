import { Container } from "react-bootstrap";
import AppNavBar from "./components/AppNavBar";
import "./App.css"; //from React
import Home from "./pages/Home";
import Courses from "./pages/Courses";
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

function App() {
  return (
    <>
      <AppNavBar />
      <Container>
        <Home />
        <Courses />
      </Container>
    </>
  );
}

export default App;

// if its from react u need closing pair tags
//if its from component it is self closing.
