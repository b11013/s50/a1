import { Button, Row, Col, Card } from "react-bootstrap";
export default function Banner() {
  return (
    <Row>
      <Col className="p-5">
        <h1> Booking App </h1> <Card.Text> Enroll courses here </Card.Text>
        <Button variant="primary"> Enroll Now </Button>
      </Col>
    </Row>
  );
}
